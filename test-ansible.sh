#!/bin/bash

DOCKER_CONTAINER_NAME="ansible-test"

cd docker && docker build -t mydebian9 .

docker run -ti --privileged --name ${DOCKER_CONTAINER_NAME} -d mydebian9

cd ../ansible && ansible-playbook -i env/local_docker playbook.yml -vvv

docker stop ${DOCKER_CONTAINER_NAME}

docker rm ${DOCKER_CONTAINER_NAME}
